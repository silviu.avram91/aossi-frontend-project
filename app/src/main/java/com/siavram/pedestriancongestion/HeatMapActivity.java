package com.siavram.pedestriancongestion;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.siavram.pedestriancongestion.mapservice.MainMapService;
import com.siavram.pedestriancongestion.utils.Utils;

import org.json.JSONException;

public class HeatMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heat_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        MainMapService mapService = MainMapService.getInstance(googleMap);
        LatLng sydney = new LatLng(-34, 151);
        mapService.addMarker(sydney, "Marker in Sydney.");
        mapService.moveCamera(sydney);

        mapService.loadHeatMapPoints();
//        try {
//            mapService.loadHeatMapPoints(Utils.readItems(getResources().openRawResource(R.raw.locations)));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }
}
