package com.siavram.pedestriancongestion.locationsservice;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by siavram on 12/08/2017.
 */

public class Location {
    int userId;
    double lat;
    double lng;
    String _id;

    public int getUserId() {
        return userId;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public LatLng toLatLng() {
        return new LatLng(getLat(), getLng());
    }
}
