package com.siavram.pedestriancongestion.locationsservice;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by siavram on 12/08/2017.
 */

public class LocationsService {
    private static Retrofit retrofit;
    private static String baseUrl = "http://10.0.2.2:8080/";
    private static LocationsServiceInterface service;

    public static LocationsServiceInterface getInstance() {
        retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
        service = retrofit.create(LocationsServiceInterface.class);
        return service;
    }

    private LocationsService() {
    }

}
