package com.siavram.pedestriancongestion.locationsservice;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by siavram on 12/08/2017.
 */

public interface LocationsServiceInterface {
    String endpoint = "/locations";

    @GET(endpoint)
    Call<List<Location>> getLocations();

    @PUT(endpoint + "/{userId}")
    void updateLocation(@Path("userId") String userId, @Body Location location);
}
