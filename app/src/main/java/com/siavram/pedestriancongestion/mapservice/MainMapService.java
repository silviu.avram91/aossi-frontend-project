package com.siavram.pedestriancongestion.mapservice;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.siavram.pedestriancongestion.locationsservice.Location;
import com.siavram.pedestriancongestion.locationsservice.LocationsService;
import com.siavram.pedestriancongestion.locationsservice.LocationsServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by siavram on 22/07/2017.
 */

public class MainMapService {

    private GoogleMap googleMap;
    private static MainMapService mainMapService;

    private MainMapService(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    public void addMarker(double lat, double lng, String title) {
        addMarker(new LatLng(lat, lng), title);
    }

    public void addMarker(LatLng latLng, String title) {
        googleMap.addMarker(new MarkerOptions().position(latLng).title(title));
    }

    public void addMarker(double lat, double lng) {
        addMarker(new LatLng(lat, lng));
    }

    public void addMarker(LatLng latLng) {
        googleMap.addMarker(new MarkerOptions().position(latLng));
    }

    public void moveCamera(double lat, double lng) {
        moveCamera(new LatLng(lat, lng));
    }

    public void moveCamera(LatLng latLng) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    public void loadHeatMapPoints() {
        LocationsServiceInterface locationService = LocationsService.getInstance();
        locationService.getLocations().enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {
                loadHeatMapPoints(response.body().stream().map(Location::toLatLng).collect(Collectors.toList()));
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {
                int x;
            }
        });

    }

    public void loadHeatMapPoints(List<LatLng> listOfHeatPoints) {
        HeatmapTileProvider provider = new HeatmapTileProvider.Builder().data(listOfHeatPoints).build();
        googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(provider));
    }

    public static MainMapService getInstance(GoogleMap googleMap) {
        if (mainMapService == null) {
            mainMapService = new MainMapService(googleMap);
        }

        return mainMapService;
    }


}
